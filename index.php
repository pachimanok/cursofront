<?php session_start();?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <title>Curso de Programación</title>
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- Estilos de Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container">
                <a class="navbar-brand" href="#">Curso de Programacion</a>
                <div class="justify-content-end">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#plan">Plan de
                                    Estudio</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tecnologias">tecnologias</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container contHead">
            <div class="row mt-5 pb-5">
                <div class="col-sm-6 ml-5 p-5 text-center">
                    <h1>Bienvenidos al Curso</h1>
                    <h3>Un curso de 5 clases</h3>
                </div>
                <div class="col col-sm-6 text-center">
                    <img src="assets/img/logoNegro.png" alt="" class="img-fluid">
                </div>

            </div>
        </div>
    </header>
    <section id="plan">
        <div class="container mt-5">
            <h1 class="text-success text-center mb-5">Plan del Curso</h1>
            <div class="col-sm-8 mx-auto">
                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Primera Clase
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse
                                collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <strong>En este curso veremos como modificar
                                    la vista del Usuario.</strong>
                                buscaremos armar nuestra propia web,
                                utilizando HTML y CSS. Nos apoyaremos en una
                                libreria llamada bootstrap.
                                en este momento estamos utilizando una clase
                                de bootstrap llamada acordion y se utiliza
                                dentro de la etiqueta agregando una clase
                                <code>.acordion</code>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Segunda Clase :: PHP Nativo
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse
                                collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <strong>This is the second item's accordion
                                    body.</strong> It is hidden by default,
                                until the collapse plugin adds the
                                appropriate classes that we use to style
                                each element. These classes control the
                                overall appearance, as well as the showing
                                and hiding via CSS transitions. You can
                                modify any of this with custom CSS or
                                overriding our default variables. It's also
                                worth noting that just about any HTML can go
                                within the <code>.accordion-body</code>,
                                though the transition does limit overflow.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Tercera Clase :: Laravel Basico
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse
                                collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <strong>This is the third item's accordion
                                    body.</strong> It is hidden by default,
                                until the collapse plugin adds the
                                appropriate classes that we use to style
                                each element. These classes control the
                                overall appearance, as well as the showing
                                and hiding via CSS transitions. You can
                                modify any of this with custom CSS or
                                overriding our default variables. It's also
                                worth noting that just about any HTML can go
                                within the <code>.accordion-body</code>,
                                though the transition does limit overflow.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingFour">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Cuarta Clase :: Laravel Basico
                            </button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse
                                collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <strong>This is the third item's accordion
                                    body.</strong> It is hidden by default,
                                until the collapse plugin adds the
                                appropriate classes that we use to style
                                each element. These classes control the
                                overall appearance, as well as the showing
                                and hiding via CSS transitions. You can
                                modify any of this with custom CSS or
                                overriding our default variables. It's also
                                worth noting that just about any HTML can go
                                within the <code>.accordion-body</code>,
                                though the transition does limit overflow.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="tecnologias" style="background: #329b3a7a;">
        <div class="container ml-5 mr-5 pl-5 mr-5">
            <h1 class="text-dark text-center pt-5 mt-5 mb-5">Tecnologias que aprenderemos</h1>
            <div class="row text-center mb-3">
                <div class="col-sm-4 text-center">
                    <div class="card m-5" style="border:none; background: transparent !important;">
                        <img src="assets/img/1.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">HTML</h5>
                            <p class="card-text">Para el front de la aplicacion</p>
                            <a href="#" class="btn btn-outline-primary">Conocer mas</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card m-5" style="border:none;">
                        <img src="assets/img/2.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">CSS</h5>
                            <p class="card-text">Para cambiar estilos del codigo</p>
                            <a href="#" class="btn btn-outline-primary">Conocer mas</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card m-5" style="border:none;">
                        <img src="assets/img/3.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Bootstrap</h5>
                            <p class="card-text">Libreria CSS HTML Y JS</p>
                            <a href="#" class="btn btn-outline-primary">Conocer mas</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="card m-5" style="border:none;">
                        <img src="assets/img/4.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <p class="card-text">Nativo para Darle dinamismos a mi Web</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card m-5" style="border:none;">
                        <img src="assets/img/5.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <p class="card-text">Para Alojar la base de datos</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card m-5" style="border:none;">
                        <img src="assets/img/6.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <p class="card-text">Framework de PHP donde haré mi proyecto</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="contacto">

        <h1 class="text-center text-dark mt-5 pt-3 pb-4">Contacto</h1>
        <?php if (isset($_SESSION['message'])){ ?>
        <div class="row">
            <div class="col-sm-5 mx-auto">
                <div class="alert alert-<?php echo $_SESSION['message_type'];?> alert-dismissible fade show" role="alert">
                   <?php echo $_SESSION['message'] ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="container">
            <div class="col-sm-8 mx-auto">
                <form action="controller/mensaje.php" method="POST">
                    <div class="d-flex">
                        <div class="col-sm-6 p-3">
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre y Apellido"
                                required>
                        </div>
                        <div class="col-sm-6 p-3">
                            <input type="number" class="form-control" name="telefono" placeholder="celular" required>
                        </div>
                    </div>
                    <div class="d-flex">
                        <input type="email" class="form-control m-3" name="email" placeholder="email" required>
                    </div>
                    <div class="d-flex">
                        <textarea id="" cols="30" rows="5" name="mensaje" placeholder="Tu Mensaje"
                            class="form-control m-3"></textarea>
                    </div>
                    <div class="d-flex m-5">
                        <button class="btn btn-success col-sm-3 mx-auto" name="enviar" type="submit">Enviar</button>
                    </div>
            </div>
            </form>
        </div>

    </section>


    <footer class="bg-dark">

    </footer>

    <!-- Scripts Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
</body>

</html>